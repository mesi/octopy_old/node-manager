import json
import argparse
import logging
import traceback
from time import sleep
from lib.octopyapp.octopyapp import OctopyApp
from lib.secop.secnode import SecNode
from lib.secop.module import Module
from lib.secop.accessible import Accessible
from lib.secop import errors as secop_errors

APP_ID = 'node manager'
LOG_LEVEL = logging.DEBUG


class NodeManager(OctopyApp):
    def __init__(self, config):
        super().__init__(APP_ID, LOG_LEVEL, config)
        self.node = None
        self.subscribe(self.topics['node manager']['update'], self.on_mqtt_refresh_message)
        self.subscribe(self.topics['node manager']['set node'], self.on_mqtt_set_node_message)
        self.subscribe(self.topics['node manager']['delete node'], self.on_mqtt_delete_node_message)
        self.subscribe(self.topics['node manager']['load node'], self.on_mqtt_load_node_message)


    def start(self):
        super().start(start_loop = False)
        # Start MQTT loop in the background
        self.mqtt_client.loop_start()
        # If we have no node loaded or added we will keep trying to load it.
        # This code is subject to race conditions where a new node is received from
        # the GUI at the same time as a response from the Storage Manager is received.
        # But since the wait is only meant to safe-guard against the Storage Manager
        # being slow at startup it shouldn't pose a real problem (fingers crossed).
        while not self.node:
            sleep(2)
            if not self.node:
                self.load_node()
        # Node loaded. Weäre done in this thread so just sleep indeifinitely
        # and let the MQTT main loop thread handle everything.
        while True:
            sleep(1000)


    def on_connect(self, client, userdata, flags, rc):
        super().on_connect(client, userdata, flags, rc)
        self.load_node()


    def on_mqtt_refresh_message(self, client, userdata, msg):
        self.load_node()


    def on_mqtt_load_node_message(self, client, userdata, msg):
        logging.info(f"Received node config from storage manager")
        try:
            payload = json.loads(msg.payload)
        except Exception as err:
            logging.error(f"Failed to parse node JSON data: {err}")
            return
        if not payload or 'error' in payload:
            # No config received or the Storage Manager returned an error.
            # We need better error handling but for now publish null.
            logging.error(f"No node configuration received, either because it's missing or there was an error")
            topic = self.topics['node manager']['structure']
            self.publish(topic = topic, payload = None, retain = True)
            return
        try:
            new_node = SecNode(payload['value'])
            # Overwrite node version with actual Octopy version
            if 'version' in self.config:
                new_node.firmware = self.config['version']
                self.node = new_node
                logging.info(f"Node '{new_node.equipment_id}' loaded")
                self.publish_node()
        except BaseException as err:
            logging.error(f"Failed to load node: {err}")
            traceback.print_exc()


    def on_mqtt_set_node_message(self, client, userdata, msg):
        try:
            payload = json.loads(msg.payload)
        except Exception as err:
            logging.error(f"Failed to parse node JSON data: {err}")
            return
        try:
            new_node = SecNode(payload)
            # Overwrite node version with actual Octopy version
            if 'version' in self.config:
                new_node.firmware = self.config['version']
            self.node = new_node
            # Store node in Storage Manager
            cmd = json.dumps({
                'category': 'nodes',
                'key': self.config['system id'],
                'value': new_node.serialize()
            }, indent = 4)
            self.publish(topic = self.topics['storage']['set'], payload = cmd)
            logging.info(f"Node '{new_node.equipment_id}' saved")
            self.publish_node()
        except BaseException as err:
            logging.error(f"Failed to load node: {err}")


    def on_mqtt_delete_node_message(self, client, userdata, msg):
        logging.error(f"Deleting node is not supported")


    def load_node(self):
        """Sends a request to the storage manager to retreive a node"""
        node_id = self.config['system id']
        # Load system node configuration
        cmd = json.dumps({
            'category': 'nodes',
            'key': node_id,
            'return topic': self.topics['node manager']['load node']
        }, indent = 4)
        logging.info(f"Loading main node: 'nodes/{node_id}'")
        self.publish(topic = self.topics['storage']['get'], payload = cmd)


    def publish_node(self):
        """Publishes a node on the structure report topic to the rest of Octopy"""
        try:
            logging.debug(f"Serializing node '{self.node.equipment_id}'")
            raw_report = self.node.serialize()
            json_report = json.dumps(raw_report, indent = 4)
            topic = self.topics['node manager']['structure']
            logging.info(f"Publishing structure report for node '{self.node.equipment_id}'")
            self.publish(topic = topic, payload = json_report, retain = True)
        except Exception as err:
            logging.error(f"Failed to publish SECoP structure report for node: {err}")


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--host',               help="MQTT host/IP", type=str, default="localhost")
    arg_parser.add_argument('--port',               help="MQTT port", type=int, default=1883)
    arg_parser.add_argument('--username', '--user', help="MQTT username", type=str, default="")
    arg_parser.add_argument('--password', '--pass', help="MQTT password", type=str, default="")
    arg_parser.add_argument('--prefix',             help="MQTT prefix", type=str, default="")
    args = arg_parser.parse_args()
    config = {
        'mqtt': {}
    }
    if args.host:
        config['mqtt']['host'] = args.host
    if args.port:
        config['mqtt']['port'] = args.port
    if args.username:
        config['mqtt']['username'] = args.username
    if args.password:
        config['mqtt']['password'] = args.password
    if args.prefix:
        config['mqtt']['prefix'] = args.prefix

    nm = NodeManager(config)
    nm.start()
