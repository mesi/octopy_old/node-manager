# Node Manager
## Installation
Requires Python 3.6
### Required Python packages
* paho-mqtt
* ujson

## Configuration
Configuration is done in the config repo and loaded through the config-loader module.
However the only configuration parameters apart from the shared MQTT-settings are
which topics to use for the API and these topics should not be reconfigured unless
a restructuring of Octopy is done. So leave the default values as they are.

Example configuration:

```javascript
{
    "node manager": {
        "topics": {
            "publish structure": "$PREFIX/octopy/structure",

            "add node": "$PREFIX/octopy/nodemanager/node/add",
            "delete node": "$PREFIX/octopy/nodemanager/node/delete",
            "attach module to node": "$PREFIX/octopy/nodemanager/node/attach",
            "detach module from node": "$PREFIX/octopy/nodemanager/node/detach",
            "load node": "$PREFIX/octopy/nodemanager/node/load",

            "add module": "$PREFIX/octopy/nodemanager/module/add",
            "delete module": "$PREFIX/octopy/nodemanager/module/delete",
            "attach accessible to module": "$PREFIX/octopy/nodemanager/module/attach",
            "detach accessible from module": "$PREFIX/octopy/nodemanager/module/detach",
            "load module": "$PREFIX/octopy/nodemanager/module/load",

            "add accessible": "$PREFIX/octopy/nodemanager/accessible/add",
            "delete accessible": "$PREFIX/octopy/nodemanager/accessible/delete",
            "attach channel to accessible": "$PREFIX/octopy/nodemanager/accessible/attach",
            "detach channel from accessible": "$PREFIX/octopy/nodemanager/accessible/attach",
            "load accessible": "$PREFIX/octopy/nodemanager/accessible/load"
        }
    }
}
```

## Running
Start the program by executing the `start.sh` shell script, alternatively use
`python3 node-manager.py`
